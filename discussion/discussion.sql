-- SQL Syntaxes are not case sensitive.
-- If you are going to add a SQL syntax(capitalize)
-- If you are going to use or add name of the column or table(small letter)

-- To show all the list of our databases:
SHOW DATABASES;

-- To create or add database

CREATE DATABASE music_db;

CREATE TABLE users(
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    username varchar(50) NOT NULL,
    password varchar(50) NOT NULL,
    full_name varchar(50) NOT NULL,
    contact_number int NOT NULL,
    email varchar(50),
    address varchar(50)
);

CREATE TABLE album(
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name varchar(64) NOT NULL,
    year date NOT NULL,
    aritst_id int NOT NULL,
    FOREIGN KEY (aritst_id) REFERENCES artists(id)
);

CREATE TABLE songs(
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title varchar(64) NOT NULL,
    length  time NOT NULL,
    genre varchar(64) NOT NULL,
    album_id int NOT NULL,
    FOREIGN KEY (album_id) REFERENCES album(id)
);

CREATE TABLE playlists(
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    datetime_created date NOT NULL,
    user_id int NOT NULL,
    FOREIGN KEY (user_ID) REFERENCES users(id) ON UPDATE cascade ON DELETE restrict
);

CREATE TABLE playlist_songs(
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    playlist_id int NOT NULL,
    song_id int NOT NULL,
    FOREIGN KEY (playlist_id) REFERENCES playlists(id)ON UPDATE cascade ON DELETE restrict,
    FOREIGN KEY (song_id) REFERENCES songs(id) ON UPDATE cascade ON DELETE restrict
);