CREATE DATABASE blog_db CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE blog_db;

CREATE TABLE IF NOT EXISTS users(
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    email varchar(100) NOT NULL,
    password varchar(300) NOT NULL,
    datetime_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP -- Optional ON UPDATE CURRENT TIMESTAMP
);

CREATE TABLE IF NOT EXISTS posts(
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title varchar(500) NOT NULL,
    content varchar(5000) NOT NULL,
    datetime_posted TIMESTAMP DEFAULT CURRENT_TIMESTAMP,-- Optional ON UPDATE CURRENT TIMESTAMP
    author_id int NOT NULL,
    CONSTRAINT  FK_author_id
    FOREIGN KEY (author_id) REFERENCES users(id) ON UPDATE cascade ON DELETE restrict
);

CREATE TABLE IF NOT EXISTS post_comments(
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    content varchar(5000) NOT NULL,
    datetime_commented TIMESTAMP DEFAULT CURRENT_TIMESTAMP,-- Optional ON UPDATE CURRENT TIMESTAMP
    post_id int NOT NULL,
    user_id int NOT NULL,
    CONSTRAINT FK_post_id
    FOREIGN KEY (post_id) REFERENCES posts(id),
    CONSTRAINT FK_user_id
    FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE cascade ON DELETE restrict
);

CREATE TABLE IF NOT EXISTS post_likes(
    id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    post_id int NOT NULL,
    user_id int NOT NULL,
    datetime_liked TIMESTAMP DEFAULT CURRENT_TIMESTAMP,-- Optional ON UPDATE CURRENT TIMESTAMP
    CONSTRAINT FK_post_id
    FOREIGN KEY (post_id) REFERENCES posts(id) ON UPDATE cascade ON DELETE restrict,
    CONSTRAINT FK_user_id
    FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE cascade ON DELETE restrict
);